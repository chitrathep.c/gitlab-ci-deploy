# gitlab-ci-deploy

## How to Run

```bash
# Start Runner
docker-compose up -d --scale gitlab-runner=3

# Run register
docker exec {name_of_gitlab-runner_1} bash script-register.sh
docker exec {name_of_gitlab-runner_2} bash script-register.sh
docker exec {name_of_gitlab-runner_3} bash script-register.sh
```

## Example .gitlab-ci.yml

```yml
image:  docker:19.03

services:
  - docker:19.03-dind

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""
  IP_SERVER: 103.86.49.244
  REMOTE_USER: ubuntu

stages:
  - build

before_script:
  - 'command -v ssh-agent >/dev/null || ( apk update -y && apk add openssh-client -y )'
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - ssh-keyscan $IP_SERVER >> ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts
  - ssh $REMOTE_USER@$IP_SERVER "ls -la && pwd && whoami"

build-app:
  stage: build
  script:
    - docker -v

```
